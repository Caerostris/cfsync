/*
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

var querystring = require('querystring');
var https = require('https');

var config = {};
config.tkn = 'your_cloudflare_token';
config.email = 'your@cfmail.com';
config.zone = 'yourdomain.com';
config.domain = 'subdomain.yourdomain.com';

function cf_command(params) {
	params.tkn = config.tkn;
	params.email = config.email;
	params.z = config.zone;
	return querystring.stringify(params);
}

function cf_req_options(length) {
	return {
		host: 'www.cloudflare.com',
		path: '/api_json.html',
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': length
		}
	}
}

function cf_call_api(params, callback) {
	var cf_query = cf_command(params);
	var req = https.request(cf_req_options(cf_query.length), function(res) {
		var data = "";

		res.on('data', function (chunk) {
			data += chunk;
		});

		res.on('end', function () {
			if(callback !== undefined) {
				callback(false, JSON.parse(data));
			}
		});

		req.on('error', function(e) {
			if(callback !== undefined) {
				callback(true, e);
			}
		});
	});

	req.write(cf_query);
	req.end();
}

function cf_update_ip(ip) {
	cf_call_api({'a': 'rec_load_all'}, function(err, res) {
		if(err || res.result != 'success') {
			console.log("request error");
			process.exit(-1);
		}

		var id = 0;

		for(var i = 0; i < res.response.recs.objs.length; i++) {
			var rec = res.response.recs.objs[i];
			if(rec.name == config.domain && rec.type == 'A') {
				id = rec.rec_id;
				break;
			}
		}

		if(id == 0) {
			console.log("rec not found");
			process.exit(-1);
		}

		cf_call_api({'a': 'rec_edit', 'type': 'A', 'name': config.domain, 'ttl': '1', 'service_mode': '0', 'content': ip, 'id': id}, function(err, res)
		{
			if(err || res.result != 'success') {
				console.log("update error: " + require('util').inspect(res));
				process.exit(-1);
			}
			console.log("success");
		});
	});
}

https.request({host: 'wtfismyip.com', path: '/text'}, function(res) {
		var data = "";

		res.on('data', function (chunk) {
			data += chunk;
		});

		res.on('end', function () {
			if(data == "") {
				console.log("get ip error");
				process.exit(-1);
			}

			console.log(data);
			cf_update_ip(data.replace("\n", ""));
		});

		res.on('error', function(e) {
			console.log("get ip error");
			process.exit(-1);
		});
}).end();
