# cfsync

cfsync is a small script providing dynamic dns for cloudflare accounts.  
It was originally built for the raspberry pi but should run on any linux host.

## Installation

1. install nodejs
2. `$ git clone https://Caerostris@bitbucket.org/Caerostris/cfsync.git`
3. update the `config.` variables in cfsync.js with your credentials and domain
4. `# make install`

## LICENSE

All code published under the Mozilla Public License 2.0
